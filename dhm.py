#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 00:50:29 2017

@author: costalongam
"""

import numpy as np
import pandas as pd
import os, sys
import operator # for sorting arrays with two criteria
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from skimage import io, restoration, exposure
from tqdm import tqdm # for loading bars
import itertools as it
import glob
import re
from os.path import join, basename
from PIL import Image
from PyQt5.QtWidgets import * # for QFileDialog

# Stack class has been coded by Remi Proville
class Stack(object):
    """
    Define a stack image to handle long series of images
    """
    def __init__(self, path, pattern):
        """
        Initialize an image stack

        Parameters
        ----------
        path: str
            Path to the serie of pictures
        pattern: str
            Pattern to select the images among the files in the folder
        """
        self._path = path
        self._pattern = pattern
        # List of the paths to all the stack images
        self.images = []
        self.get_im_list()

    @property
    def path(self):
        return self._path

    @property
    def pattern(self):
        return self._pattern

    def get_im_list(self):
        """ Make a list of all pictures"""
        l_path = np.array(glob.glob(os.path.join(self.path, self.pattern)))
        l_filenames = [os.path.basename(p) for p in l_path]

        regex = re.compile('([0-9]*)')
        try:
            index = [int(''.join(regex.findall(flnm)))
                     for flnm in l_filenames]
        except ValueError:
            print("No number detected in one file")
        # get the indexes of the last number in the file name
        index = np.argsort(index)
        self.images = l_path[index]
        
    def reverse(self):
        """ Reverse the order of the images """
        self.images = self.images[::-1]

    @staticmethod
    def get_pic(im):
        """
        Load an image

        Parameters
        ----------
        im: str
            Path to the pictures

        Returns
        -------
        pic: Numpy `ndarray`
            Image
        """
        pic = np.asarray(Image.open(im))
        return pic

    def next(self):
        for im in self.images:
            yield self.get_pic(im)

    def __iter__(self):
        return self.next()

    def __getitem__(self, item):
        # Help get some iteration capabilities
        # If we want a slice, check start and stop values
        if isinstance(item, slice):
            if item.start is None:
                item = slice(0, item.stop, item.step)
            if item.stop is None:
                item = slice(item.start, -1, item.step)
        # if asking for a range: construct the corresponding slice
        elif isinstance(item, list) or isinstance(item, tuple):
            item = slice(item[0], item[1])
        # idem if asking for just one
        else:
            s = 1 if item >= 0 else -1
            item = slice(item, item+s)
        # Chekc we are in the range
        if item.stop > len(self.images):
            raise IndexError('Folder: {} contains only {} images. '
                             '{} is out of bounds'.format(self.path,
                                                          len(self.images),
                                                          item))
        else:
            # Load all images
            pics = [self.get_pic(x) for x in self.images[item]]
            # And return them as a 3D array
            return np.squeeze(pics)

    def __repr__(self):
        return 'Stack of {} images from folder {}'.format(len(self.images),
                                                          self.path)

    def __len__(self):
        return len(self.images)
    


def unwrap_im(im):
    """ 
    Unwrap a np.uint8 phase image. Attention : it doubles the phase jumps.
    
    Parameters
    ----------
    
    im: numpy.uint8 
        8-bit grayscale phase image to be unwrapped
        
    Returns
    ----------
    numpy.uint8 unwrapped image
        
    """
    im = exposure.rescale_intensity(1.0*im, in_range=(0, 255), out_range=(0, 4*np.pi))
    im = np.angle(np.exp(1j * im))
    return restoration.unwrap_phase(im)

def rescale_unwrap_im(imw, unwrap_per=4*np.pi, real_per=333):
    """ 
    Rescale an unwrap image, given the real value of a phase jump.
    
    Parameters
    ----------
    
    imw: numpy.uint8 
        8-bit grayscale unwrapped phase image
    unwrap_per: float
        period of a phase jump
        Default: 2*np.pi
    real_per: float
        real scale corresponding to a phase jump
        Default: 333/2 (half the half-wavelength of the laser)
        
    Returns
    ----------
    numpy.uint8 rescaled unwrapped image
        
    """
    ran = np.amax(imw)-np.amin(imw)
    nper = np.floor(ran/unwrap_per)
    rest = (ran - nper*unwrap_per)/ran
    return exposure.rescale_intensity(imw - np.amin(imw), 
                                      in_range=(0, ran), 
                                      out_range=(0, real_per*(nper + rest)))
    
def get_xykeys(mat, shift=(0,0)):
    """ 
    Return a list of 2-int tuples corresponding to all combinations between 
    numbers from np.arange(0,shape[0],1) and np.arange(0,shape[1],1)
    
    Parameters
    ----------
    
    shape : tuple of int, or numpy.2darray 
        typically the shape of a matrix, or the matrix itself
        
    Returns
    ----------
    list of tuple of 2 int
        
    """
    if type(mat) is np.ndarray:
        shape = mat.shape
    else:
        shape = mat
    x = np.arange(0,shape[1],1) + shift[1]
    y = np.arange(0,shape[0],1) + shift[0]
    X, Y = np.meshgrid(x, y)
    return list(it.zip_longest(list(X.flatten('F').astype('int')), list(Y.flatten('F').astype('int'))))

def get_xyz(xyz):
    """
    Return x, y and z matrices in the proper shape for a surface plot of xyz
    2d array of 3 columns, where the first two columns are x,y coordinates
    (sorted), and the third column is the corresponding z value.

    Parameters
    ----------

    xyz : numpy.2darray
        surface to be plotted, must have at least 3 columns (x, y, z), x and y
        being sorted

    Returns
    ----------
    x : numpy.ndarray
        x coordinates
    y : numpy.ndarray
        y coordinates
    z : numpy.ndarray
        surface values

    """
    if type(xyz) is dict:
        vxyz = np.array([[k[0], k[1], xyz[k]] for k in xyz.keys()])
    vxyz = np.array(sorted(xyz, key=operator.itemgetter(0, 1)))
    shape = (len(set(vxyz[:,0])), len(set(vxyz[:,1])))
#    shape = (vxyz[:,0].ptp()+1, vxyz[:,1].ptp()+1)
    x = vxyz[:,0].reshape(shape)
    y = vxyz[:,1].reshape(shape)
    z = vxyz[:,2].reshape(shape)
    return x, y, z

def im2xyz(im, shift=(0,0)):
    """ 
    Return a 3 columns array (x, y, pix) from an image im (numpy.uint8). If 
    im.shape = (m, n), then returnedArray.shape = (m*n, 3)
    
    Parameters
    ----------
    
    im : numpy.2darray 
        image
    shift : tuple of int, optional
        shift to be applied to x and y coordinates
        Default: (0, 0)
        
    Returns
    ----------
    numpy.2darray
        
    """
    keys = get_xykeys(im)
    return np.array([[i[0] + shift[0], i[1] + shift[1], im[i[1],i[0]]] for i in keys])

def stitch2d(im_stack, shift, start=0, end=None, conv=333):
    """
    Process a stack of phase images to be stitched. Returns an array of the
    mean gaps between overlapping surfaces from two consecutives images,
    and an numpy.ndarray with 3 columns x y and z corresponding to an image of
    the fully stitched stack.

    Parameters
    ----------

    im_stack : Stack object
        stack of phase images to stitch
    shift : tuple of int
        (x, y) shift between two consecutive images
    start : int, optional
        number of the starting frame
        Default: 0
    end : int, optional
        number of the ending frame
        Default: None
    conv: float, optional
        Conversion factor to apply when unwrapping images
        Default: 666/2

    Returns
    ----------
    d : numpy.ndarray
        1d array of gaps between surfaces from two consecutive images
    im_stitch : numpy.ndarray
        numpy.ndarray corresponding to an image of the fully stitched stack

    """
    if end is None:
        end = len(im_stack)
    im_stitch = rescale_unwrap_im(unwrap_im(im_stack[start]), real_per=conv)
    h, w = im_stitch.shape
    it_stack = range(start, end)
    d = [np.nan for _ in it_stack]
    d[0] = 0
    shift = np.array(shift[::-1], dtype=int)
    im_full = np.zeros((h+np.abs(shift[0])*(len(it_stack)-1), w+np.abs(shift[1])*(len(it_stack)-1)))
    r, c = im_full.shape
    if shift[0] >= 0 and shift[1] < 0:
        im_full[:h, -w:] = im_stitch
    elif shift[0] >= 0 and shift[1] >= 0:
        im_full[:h, :w] = im_stitch
    elif shift[0] < 0 and shift[1] >= 0:
        im_full[-h:, :w] = im_stitch
    elif shift[0] < 0 and shift[1] < 0:
        im_full[-h:, -w:] = im_stitch
    for i in tqdm(range(start + 1, end), desc='Stitching images'):
        c_shift = (i - start) * shift
        temp_trans = rescale_unwrap_im(unwrap_im(im_stack[i]), real_per=conv)
        im_trans = np.zeros((r, c))
        if shift[0] >= 0 and shift[1] < 0:
            im_trans[c_shift[0]:(h + c_shift[0]), -(w - c_shift[1]):-(-c_shift[1])] = temp_trans
        elif shift[0] >= 0 and shift[1] >= 0:
            im_trans[c_shift[0]:(h + c_shift[0]), c_shift[1]:(w + c_shift[1])] = temp_trans
        elif shift[0] < 0 and shift[1] >= 0:
            im_trans[-(h - c_shift[0]):-(-c_shift[0]), c_shift[1]:(w + c_shift[1])] = temp_trans
        elif shift[0] < 0 and shift[1] < 0:
            im_trans[-(h - c_shift[0]):-(-c_shift[0]), -(w - c_shift[1]):-(-c_shift[1])] = temp_trans
        mask_full_stitch = im_full.astype(bool)
        mask_full_trans = im_trans.astype(bool)
        mask_intersect = np.logical_and(mask_full_stitch, mask_full_trans)
        mask_diff = (mask_full_trans.astype(int) - mask_full_stitch.astype(int) > 0)
        d[i - start] = np.mean(im_trans[mask_intersect] - im_full[mask_intersect])
        im_trans[mask_full_trans] = im_trans[mask_full_trans] - d[i - start]
        im_full[mask_intersect] = 0.5*(im_trans[mask_intersect] + im_full[mask_intersect])
        im_full[mask_diff] = im_trans[mask_diff]
    
    return d, im_full


def plot_unstitch(im_stack, shift=(0,0), start=0, end=None):
    """
    Plot the surfaces corresponding to unwrapped phase images from stack
    im_stack, with a constant shift between images.

    Parameters
    ----------

    im_stack : Stack object
        stack of phase images
    shift : tuple of int
        (x, y) shift between two consecutive images
    start : int, optional
        number of the starting frame
        Default: 0
    end : int, optional
        number of the ending frame
        Default: None

    """
    if end is None:
        end = len(im_stack)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    maps = [cm.cool, cm.coolwarm, cm.hot]
    for i in range(start, end):
        im = rescale_unwrap_im(unwrap_im(im_stack[i]))
        xx = np.arange(0,im.shape[1],1) + (i - start) * shift[1]
        yy = np.arange(0,im.shape[0],1) + (i - start) * shift[0]
        X, Y = np.meshgrid(xx, yy)
        ax.plot_surface(X, Y, im, cmap=maps[(i - start) % 3], linewidth=0, antialiased=False)

def plot_stitch3d(im_stitch):
    """
    Plot the stitched image in 3d

    Parameters
    ----------

    im_stitch : numpy.ndarray
        numpy.ndarray with 3 columns x y and z corresponding to an image of
        the fully stitched stack
    
    Returns
    ----------
    x : numpy.ndarray
        x coordinates matrix
    y : numpy.ndarray
        y coordinates matrix
    z : numpy.ndarray
        surface values matrix

    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    im_conv = im2xyz(im_stitch)
    x, y, z = get_xyz(im_conv)
    surf = ax.plot_surface(x, y, z, cmap=cm.coolwarm,
                           vmin=np.nanmin(z), vmax=np.nanmax(z),
                           linewidth=0, antialiased=False)
    fig.show()
    return x, y, z

def load_stitch(path=None):
    """
    Load datas generated by command line dhm_stitch_cmd.

    Parameters
    ----------

    path : str
        path to the datas
        Default: None

    Returns
    ----------
    im_stitch : numpy.ndarray
        numpy.ndarray corresponding to an image of the fully stitched stack
    d : numpy.ndarray
        1d array of gaps between surfaces from two consecutive images

    """
    if path is None:
        path = QFileDialog.getExistingDirectory(caption='Select directory where dict and stitched files are ...')
    d = np.loadtxt(os.path.join(path, 'dist.txt') , delimiter='\t')
    im_stitch = np.loadtxt(os.path.join(path, 'stitched.txt') , delimiter='\t')
    return im_stitch, d
    
if __name__ == "__main__" :
    im, d = load_stitch('/Users/maximecostalonga/Science/Viscous_cheerios/stitching/testImages/')
    plot_stitch3d(im)
