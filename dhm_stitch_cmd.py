#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 02:25:08 2017

@author: costalongam
"""

import argparse # for command line execution 
import os, sys
import numpy as np
import pandas as pd

# Custom modules
from dhm import *

def get_params(argv):
    """
    Function called to read and parse command line arguments

    Arguments
    ----------
    argv: list
        Command line arguments as passed by sys.argv[1:]

    Returns
    --------
    args: Namespace object returned by `argparse`
            Contains the values for all necessary parameters

    Possible flags
    ----------------
    -p = PATH ; --path = path
    -s = START; --start = START
    -e = END ; --end = END
    -t = *.tif ; --type = *.tif
    -xs ; --xshift (shift in horizontal direction of image)
    -ys ; --yshift (shift in vertical direction of image)
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', help='Folder containing the datas, Holograms, Intensity and Phase',
                        default='.', type=str)
    parser.add_argument('-xs', '--xshift', help='Shift in x direction',
                        default=-341, type=int)
    parser.add_argument('-ys', '--yshift', help='Shift in y direction',
                        default=0, type=int)
    parser.add_argument('-e', '--end', help='Last frame to analyse',
                        default=None, type=int)
    parser.add_argument('-s', '--start', help='First frame to analyse',
                        default=0, type=int)
    parser.add_argument('-t', '--type', help='Image time (*.tif)',
                        default='*.tif', type=str)
    parser.add_argument('-ns', '--nsample', help='Sample refractive index',
                        default='1.33', type=float)
    parser.add_argument('-nref', '--nref', help='Reference refractive index',
                        default='1', type=float)
    parser.add_argument('-rev', '--reverse', action='store_true', help='Reverse the stack')
    parser.add_argument('-trans', '--transmission', action='store_true', help='DHM-T used')
    args = parser.parse_args(argv)
    args.path = os.path.normpath(os.path.expanduser(os.path.abspath(args.path)))

    return args


if __name__ == "__main__":
    """
    Typical use in the command line:
    > python dhm_stitch_cmd.py -p path/ -s 0 -e 10 -xs 300 -ys 0
    Note that the path should lead to a folder where there is at least a 
    Phase/Image/ path containing the images to be stitched
    """
    # path = '/Users/costalongam/Science/Gel_meniscus/171130_agar0015_needle300mic/'
    # Parse the command line arguments
    cl_args = get_params(sys.argv[1:])
    shift = (cl_args.xshift, cl_args.yshift)
    # Make a stack
    phase_path = os.path.join(cl_args.path, 'Phase/Image/')
    im_stack = Stack(phase_path, cl_args.type)
    if cl_args.reverse:
        im_stack.reverse() # reverse the order of the stack
    # Sticht unwrapped phase images
    if cl_args.transmission:
        conv = 666/(cl_args.nsample - cl_args.nref)
    else:
        conv = 666/(2*cl_args.nref)
    dist, im_stitch = stitch2d(im_stack, shift, cl_args.start, cl_args.end, conv)
    # Saving
    print('Saving...')
    np.savetxt(os.path.join(cl_args.path, 'dist.txt'), dist, delimiter='\t')
    np.savetxt(os.path.join(cl_args.path, 'stitched.txt'), im_stitch, delimiter='\t')
    print('Done.')
