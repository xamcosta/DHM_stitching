#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  6 00:50:29 2017

@author: costalongam
"""

########################################
########################################    
########### Old functions ###########
########################################
########################################

import numpy as np
import pandas as pd
import os, sys
import operator # for sorting arrays with two criteria
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
from skimage import io, restoration, exposure
from tqdm import tqdm # for loading bars
import itertools as it
import glob
import re
from os.path import join, basename
from PIL import Image
from PyQt5.QtWidgets import * # for QFileDialog


def im2xydict(im, shift=(0,0)):
    """ 
    Return a dict where keys are tuple of coordinates and values are pixel 
    values from an image im (numpy.uint8). 
    
    Parameters
    ----------
    
    im : numpy.2darray 
        image
    shift : tuple of int, optional
        shift to be applied to x and y coordinates
        Default: (0, 0)
        
    Returns
    ----------
    dict with format (x,y):pix
        
    """
    keys = get_xykeys(im)
    b = dict()
    for (xx, yy) in keys:
        b[(int(xx + shift[1]), int(yy + shift[0]))] = im[yy, xx]
    return b

def stitch2d_dict(im_stack, shift):
    """ 
    Process a stack of phase images to be stitched. Returns an array of the  
    mean gaps between overlapping surfaces from two consecutives images, 
    and an numpy.ndarray with 3 columns x y and z corresponding to an image of 
    the fully stitched stack.
    
    Parameters
    ----------
    
    im_stack : Stack object 
        stack of phase images to stitch
    shift : tuple of int
        (x, y) shift between two consecutive images
        
    Returns
    ----------
    d : numpy.ndarray
        1d array of gaps between surfaces from two consecutive images
    im_stitch : numpy.ndarray
        numpy.ndarray with 3 columns x y and z corresponding to an image of 
        the fully stitched stack
        
    """
    im_stitch = im2xydict(rescale_unwrap_im(unwrap_im(im_stack[0])))
    d = [None for _ in range(0, len(im_stack))]
    d[0] = 0
    for i in tqdm(range(1, len(im_stack)), desc='Stitching images'):
        im_trans = im2xydict(rescale_unwrap_im(unwrap_im(im_stack[i])), 
                             (int(i*shift[1]), int(i*shift[0])))
        d[i] = np.mean([im_trans[k] - im_stitch[k] for k in set(im_stitch.keys()).intersection(im_trans.keys())])
        im_trans.update({k: v - d[i] for k, v in im_trans.items()})
        for k in im_trans.keys():
            if im_stitch.get(k):
                im_stitch[k] = 0.5*(im_stitch[k] + im_trans[k])
            else:
                im_stitch[k] = im_trans[k]
           
    keyshift = tuple([0 if x > 0 else (len(im_stack) - 1)*x for x in reversed(shift)])
    keylist = get_xykeys((int(im_stack[0].shape[0] + (len(im_stack) - 1)*np.abs(shift[1])), int(im_stack[0].shape[1] + (len(im_stack) - 1)*np.abs(shift[0]))), 
                         keyshift)
    for k in set(keylist).difference(im_stitch.keys()):
        im_stitch[k] = np.nan
     
    xyz_stitch = np.array([[k[0], k[1], im_stitch[k]] for k in tqdm(im_stitch.keys(), desc='Generating stichted image')])
    print('Finalisation...')
    return np.array(d), np.array(sorted(xyz_stitch, key=operator.itemgetter(0, 1)))

def im2pd(im, shift=(0,0)):
    """ 
    Return a pandas.DataFrame where index are tuple of coordinates and columns 
    are x, y coordinates and pixel values from an image im (numpy.uint8). 
    
    Parameters
    ----------
    
    im : numpy.2darray 
        image
    shift : tuple of int, optional
        shift to be applied to x and y coordinates
        Default: (0, 0)
        
    Returns
    ----------
    pandas.DataFrame with columns=['x', 'y', 'z']
        
    """
    keys = get_xykeys(im)
    b = []
    for xx, yy in keys:
        b.append(pd.DataFrame(data={'x':int(xx + shift[0]), 'y':int(yy + shift[1]), 'z':im[xx, yy]}, index=[(xx, yy)]))
        
    return pd.concat(b)

def make_stitch_pd(im_stack, shift):
    """ 
    Process a stack of phase images to be stitched. Returns an array of the  
    mean gaps between overlapping surfaces from two consecutives images, 
    and an numpy.ndarray with 3 columns x y and z corresponding to an image of 
    the fully stitched stack.
    
    Parameters
    ----------
    
    im_stack : Stack object 
        stack of phase images to stitch
    shift : tuple of int
        (x, y) shift between two consecutive images
        
    Returns
    ----------
    d : numpy.ndarray
        1d array of gaps between surfaces from two consecutive images
    im_stitch : pandas.DataFrame
        pandas.DataFrame with columns=['x', 'y', 'z'] corresponding to an 
        image of the fully stitched stack
        
    """
    im_stitch = im2pd(rescale_unwrap_im(unwrap_im(im_stack[0])))
    d = [None for _ in range(0, len(im_stack))]
    d[0] = 0
    for i in tqdm(range(1, len(im_stack)), desc='Stitching images'):
        im_trans = im2pd(rescale_unwrap_im(unwrap_im(im_stack[i])), 
                             (int(i*shift[0]), int(i*shift[1])))
        d[i] = np.mean([im_trans['z'].loc[[k]].values[0] - im_stitch['z'].loc[[k]].values[0] for k in set(im_stitch.index).intersection(im_trans.index)])
        im_trans['z'] = im_trans['z'].apply(lambda x: x - d[i])
        for k in set(im_stitch.index).intersection(im_trans.index):
            im_stitch.at[k, 'z'] = 0.5*(im_stitch['z'].loc[[k]].values[0] + im_trans['z'].loc[[k]].values[0])
        for k in set(im_trans.index).intersection(im_stitch.index):
            im_stitch.append(im_trans.loc[[k]])
           
        im_stitch = pd.concat(im_stitch)
    return np.array(d), im_stitch.sort_values(['x', 'y'], ascending=[True, True])
    
    