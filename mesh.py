from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
import pyqtgraph.opengl as gl
from dhm import *
import sys

path = "exp_bidon/Phase/Image"
im_stack = Stack(path, "*.tif")
im_stitch = rescale_unwrap_im(unwrap_im(im_stack[0]))
t, b = im_stitch.max(), im_stitch.min()
im_stitch = 100 * (im_stitch - t) / (t - b)




if __name__ == '__main__':
    app = QtGui.QApplication([])


    w = gl.GLViewWidget()
    w.show()
    w.setWindowTitle('Surface plot')
    w.setCameraPosition(distance=40)

    g = gl.GLGridItem()
    g.scale(50,50,1)
    w.addItem(g)

    m1 = gl.GLSurfacePlotItem(z=im_stitch, shader='shaded')
    m1.translate(-im_stitch.shape[0]/2, -im_stitch.shape[1]/2, -im_stitch.mean())
    # m1.setGLOptions('additive')
    w.addItem(m1)
    sys.exit(app.exec_())
